#!/bin/bash

set -e
#system update
#sudo pacman -Syyu --noconfirm

#add new user
sudo useradd -m -G wheel -s /bin/bash ostry
sudo passwd ostry

sudo visudo

#xinit starts graphic einv.
sudo pacman -S --noconfirm --needed xorg-xinit
sudo cp /etc/X11/xinit/xinitrc ~/.xinitrc

#update xinitrc to start bspwm 

sed -i '51,55d' ~/.xinitrc  
echo "exec bspwm" >> ~/.xinitrc

#install fish
sudo pacman -S --noconfirm --needed fish
chsh -s /usr/bin/fish
#programs installation

#sudo pacman -S --noconfirm --needed vim
#sudo pacman -S --noconfirm --needed cmus 
#sudo pacman -S --noconfirm --needed yay 
#sudo pacman -S --noconfirm --needed keepassxc 
#sudo pacman -S --noconfirm --needed gimp 
#sudo pacman -S --noconfirm --needed inkscape 
#sudo pacman -S --noconfirm --needed vlc 
#sudo pacman -S --noconfirm --needed mpv 
#sudo pacman -S --noconfirm --needed veracrypt 
#sudo pacman -S --noconfirm --needed thunderbird 
#sudo pacman -S --noconfirm --needed newsboat 
#sudo pacman -S --noconfirm --needed feh 
#sudo pacman -S --noconfirm --needed rofi 
#sudo pacman -S --noconfirm --needed firefox 
#sudo pacman -S --noconfirm --needed firejail 
#sudo pacman -S --noconfirm --needed resolvconf 
#sudo pacman -S --noconfirm --needed wireguard-tools wireguard-dkms 
#sudo pacman -S --noconfirm --needed ranger 



#yay -S --noconfirm --needed viber

echo "#####################################"
echo "             REBOOT - FISH           "
echo "#####################################"
