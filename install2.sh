#!/bin/bash

set -e
#create fish config
#sudo mkdir ~/.config/fish/
#sudo touch ~/.config/fish/config.fish

#update fish to startx after login
echo "# Start X at login" >> ~/.config/fish/config.fish
echo "if status is-login" >> ~/.config/fish/config.fish
echo "    if test -z \"$DISPLAY\" -a $XDG_VTNR = 1" >> ~/.config/fish/config.fish
echo "        exec startx -- -keeptty" >> ~/.config/fish/config.fish
echo "    end" >> ~/.config/fish/config.fish
echo "end" >> ~/.config/fish/config.fish

#Install I3
sudo pacman -S bspwm sxhkd


sudo mkdir ~/.config/bspwm
sudo mkdir ~/.config/sxhkd
sudo cp /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/
sudo cp /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/



