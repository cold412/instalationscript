﻿#!/bin/bash

sudo pacman -S --noconfirm --needed xorg-xinit fish bspwm sxhkd rxvt-unicode yay
#yay -S polibar

sudo mkdir /home/ostry/.config/sxhkd
sudo mkdir /home/ostry/.config/bspwm
sudo cp /usr/share/doc/bspwm/examples/bspwmrc /home/ostry/.config/bspwm/
sudo cp /usr/share/doc/bspwm/examples/sxhkdrc /home/ostry/.config/sxhkd/



#xinit starts graphic einv.
sudo cp /etc/X11/xinit/xinitrc /home/ostry/.xinitrc

#update xinitrc to start bspwm 
sed -i '51,55d' /home/ostry/.xinitrc  
echo "exec bspwm" >> /home/ostry/.xinitrc

#fish
#sudo pacman -S --noconfirm --needed fish
#chsh -s /usr/bin/fish

                                                                                                                                                                                                  
 
set -e
#create fish config
#sudo mkdir /home/ostry/.config/fish/
sudo touch /home/ostry/.config/fish/config.fish
#update fish to startx after login

sudo echo "# Start X at login" >> /home/ostry/.config/fish/config.fish
sudo echo "if status is-login" >> /home/ostry/.config/fish/config.fish
sudo echo "    if test -z \"$DISPLAY\" -a $XDG_VTNR = 1" >> /home/ostry/.config/fish/config.fish
sudo echo "        exec startx -- -keeptty" >> /home/ostry/.config/fish/config.fish
sudo echo "    end" >> /home/ostry/.config/fish/config.fish
sudo echo "end" >> /home/ostry/.config/fish/config.fish


 
